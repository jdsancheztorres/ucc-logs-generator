package co.edu.campusucc.business;

import java.io.File;
import java.nio.charset.StandardCharsets;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import co.edu.campusucc.domain.Email;

/**
 * Service class used for sending emails for some user on the UCCLogsGenerator
 * project.
 * 
 * @author jose.sanchezt@campusucc.edu.co
 *
 */
@Service
public class EmailService extends GenericService implements IEmailService {

	private static final Logger LOGGER = LoggerFactory.getLogger(EmailService.class);

	@Autowired
	private JavaMailSender javaMailSender;

	@Autowired
	private SpringTemplateEngine templateEngine;

	@Override
	public String sendSimpleMail(final Email email) {

		String emailMessage = "";
		try {
			emailMessage = sendSimpleMailDetail(email);
		} catch (Exception e) {
			emailMessage = "Error while Sending Mail";
			e.printStackTrace();
		}

		return emailMessage;
	}

	@Override
	public String sendMailWithAttachment(final Email email) {
		String emailMessage = "";
		try {
			emailMessage = sendMailWithAttachmentDetail(email);
		} catch (Exception e) {
			emailMessage = "Error while Sending Mail with Attachment - Exception: " + e.toString();
		}

		LOGGER.info("Status message ... " + emailMessage);

		return emailMessage;
	}

	private String sendSimpleMailDetail(final Email email) {

		SimpleMailMessage mailMessage = new SimpleMailMessage();
		mailMessage.setFrom(sender);
		mailMessage.setTo(email.getRecipient());
		mailMessage.setText(email.getMsgBody());
		mailMessage.setSubject(email.getSubject());
		javaMailSender.send(mailMessage);
		return "Mail Sent Successfully...";
	}

	private String sendMailWithAttachmentDetail(final Email email) throws MessagingException {

		LOGGER.info(EmailService.class.getCanonicalName() + "::sendMailWithAttachmentDetail()   - BEGIN");

		final MimeMessage mimeMessage = javaMailSender.createMimeMessage();

		final MimeMessageHelper mimeMessageHelper;
		mimeMessageHelper = new MimeMessageHelper(mimeMessage, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());
		mimeMessageHelper.setFrom(sender);
		mimeMessageHelper.setTo(email.getRecipient());
		mimeMessageHelper.setText(email.getMsgBody());
		mimeMessageHelper.setSubject(email.getSubject());
		final String emails[] = new String[2];
		// emails[0] = this.teacher1;
		// emails[1] = this.teacher2;
		emails[0] = this.student1;
		emails[1] = this.student2;
		mimeMessageHelper.setCc(emails);
		mimeMessageHelper.setBcc(this.student1);
		mimeMessageHelper.setText(templateEngine.process("UCCEmail", createContext(email)), true);
		final FileSystemResource file = new FileSystemResource(new File(email.getAttachment()));
		mimeMessageHelper.addAttachment(file.getFilename(), file);
		javaMailSender.send(mimeMessage);

		LOGGER.info(EmailService.class.getCanonicalName() + "::sendMailWithAttachmentDetail()   - END");

		return "Mail with attachment sent Successfully";
	}

	private Context createContext(final Email email) {
		final Context context = new Context();
		context.setVariable("name", "Estimado(a) " + email.getName() + " " + email.getLastName() + ": ");
		context.setVariable("topics", email.getTopics().toArray());
		return context;
	}

}
