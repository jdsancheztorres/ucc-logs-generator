package co.edu.campusucc.business;

import java.util.stream.Stream;

import co.edu.campusucc.domain.Topic;

public interface ITopicService {
	Stream<Topic> findAll();
}