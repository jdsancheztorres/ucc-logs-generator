package co.edu.campusucc.business;

import java.util.Optional;

import co.edu.campusucc.api.payload.APIUserLogRequest;
import co.edu.campusucc.domain.UserLog;
/**
 * 
 * Interface component for invoking the UserLogsService implementation.
 * @author jose.sanchezt@campusucc.edu.co
 *
 */
public interface IUserLogService {
	Optional<UserLog> save(final APIUserLogRequest apiUserRequest);
}
