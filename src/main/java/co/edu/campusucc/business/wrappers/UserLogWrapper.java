package co.edu.campusucc.business.wrappers;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import co.edu.campusucc.api.payload.APIUserLogRequest;
import co.edu.campusucc.api.payload.UserLogConverter;
import co.edu.campusucc.business.IEmailService;
import co.edu.campusucc.common.UCCUtil;
import co.edu.campusucc.domain.Email;
import co.edu.campusucc.domain.UserLog;
import co.edu.campusucc.infra.spring.data.PositionEntity;
import co.edu.campusucc.infra.spring.data.PositionRepository;
import co.edu.campusucc.infra.spring.data.TopicEntity;
import co.edu.campusucc.infra.spring.data.TopicRepository;
import co.edu.campusucc.infra.spring.data.UserLogEntity;
import co.edu.campusucc.infra.spring.data.UserLogRepository;
import lombok.Getter;
import lombok.Setter;

/**
 * Wrapper class used for handling operations associated to the User Log.
 * 
 * @author jose.sanchezt@campusucc.edu.co
 *
 */
@Component
@Getter
@Setter
public class UserLogWrapper implements IUserLogWrapper {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserLogWrapper.class);

	@Autowired
	private UserLogRepository userLogRepository;

	@Autowired
	private PositionRepository positionTypeRepository;

	@Autowired
	private TopicRepository topicRepository;

	@Autowired
	private IEmailService emailService;

	@Value("${spring.path.logs}")
	private String logsPath;

	public Optional<UserLog> save(final APIUserLogRequest apiUserRequest) {

		LOGGER.info(UserLogWrapper.class.getCanonicalName() + "::save()   - BEGIN");
		final Optional<UserLog> optionalUser = createUserLog(apiUserRequest);
		sendEmail(apiUserRequest);
		LOGGER.info(UserLogWrapper.class.getCanonicalName() + "::save()   - END");

		return optionalUser;
	}

	private Optional<UserLog> createUserLog(final APIUserLogRequest apiUserRequest) {

		final UserLogEntity entity = UserLogConverter.toUserEntity(apiUserRequest);
		Optional<PositionEntity> positionEntity = null;
		positionEntity = positionTypeRepository.findById(apiUserRequest.getUserPosition());
		entity.setPosition(positionEntity.get());
		final List<String> topics = apiUserRequest.getTopics();
		Set<TopicEntity> likes = new HashSet<TopicEntity>();
		for (String topic : topics) {
			likes.add(topicRepository.findByName(topic).get());
		}
		entity.setLikes(likes);
		userLogRepository.save(entity);

		LOGGER.info("entity a enviar es " + entity.toString());

		Optional<UserLog> optionalUser = Optional.of(UserLogConverter.toUser(entity));
		return optionalUser;
	}

	private String generateLog(final APIUserLogRequest apiUserRequest) {

		final String pathLogsFolder = logsPath;
		final String sourceLogs = pathLogsFolder + "/ucc.log";
		final String user = UCCUtil.retrieveName(apiUserRequest.getName(), apiUserRequest.getLastName());
		final String targetLogs = pathLogsFolder + "/" + UCCUtil.normalizeString(apiUserRequest.getCompany()) + "_" + user + ".log";

		try {
			List<String> logsRecords = UCCUtil.readFileByCriteria(sourceLogs, user);
			UCCUtil.writeFile(targetLogs, logsRecords);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return targetLogs;
	}

	private String sendEmail(final APIUserLogRequest apiUserRequest) {

		LOGGER.info(UserLogWrapper.class.getCanonicalName() + "::sendEmail()   - START");

		final Email email = new Email();
		email.setName(apiUserRequest.getName());
		email.setLastName(apiUserRequest.getLastName());
		email.setMsgBody("Hola, esta es una prueba utilizando el " + "proyecto UCCLogsGenerator para mi tesis."
				+ "\nMuchas gracias\nSaludos.\n\\nJosé Danilo");
		email.setRecipient(apiUserRequest.getEmail());
		email.setSubject("Log generado - Proyecto UCC Logs Generator");
		email.setAttachment(generateLog(apiUserRequest));
		email.setTopics(apiUserRequest.getTopics());
		LOGGER.info(UserLogWrapper.class.getCanonicalName() + "::sendEmail()   - END");

		return emailService.sendMailWithAttachment(email);
	}
}
