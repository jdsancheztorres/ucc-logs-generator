package co.edu.campusucc.business.wrappers;

import java.util.Optional;

import co.edu.campusucc.api.payload.APIUserLogRequest;
import co.edu.campusucc.domain.UserLog;

public interface IUserLogWrapper {
	
	Optional<UserLog> save(final APIUserLogRequest apiUserLogRequest);
}
