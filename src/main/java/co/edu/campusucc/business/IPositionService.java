package co.edu.campusucc.business;

import java.util.stream.Stream;

import co.edu.campusucc.domain.Position;

public interface IPositionService {

	Stream<Position> findAllOrderByName();
}
