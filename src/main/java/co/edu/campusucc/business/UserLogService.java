package co.edu.campusucc.business;

import java.util.Optional;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.campusucc.api.payload.APIUserLogRequest;
import co.edu.campusucc.business.wrappers.IUserLogWrapper;
import co.edu.campusucc.domain.UserLog;

@Service
public class UserLogService implements IUserLogService {
	
	static final Logger LOGGER = Logger.getLogger(UserLogService.class.getCanonicalName());
	
	@Autowired
	private IUserLogWrapper userWrapper;

	public Optional<UserLog> save(final APIUserLogRequest apiUserRequest) {
		try {
			return userWrapper.save(apiUserRequest	);
		} catch (Exception ex) {
			throw ex;
		}
	}
}
