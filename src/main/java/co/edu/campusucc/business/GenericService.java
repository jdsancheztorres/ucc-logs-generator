package co.edu.campusucc.business;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class GenericService {

	@Value("${spring.mail.username}")
	protected String sender;
	
	@Value("${spring.mail.student1}")
	protected String student1;
	
	@Value("${spring.mail.student2}")
	protected String student2;
	
	@Value("${spring.mail.teacher1}")
	protected String teacher1;
	
	@Value("${spring.mail.teacher2}")
	protected String teacher2;
}
