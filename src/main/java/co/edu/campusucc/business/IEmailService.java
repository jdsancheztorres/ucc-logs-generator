package co.edu.campusucc.business;

import co.edu.campusucc.domain.Email;

public interface IEmailService {

    String sendSimpleMail(final Email details);
 
    String sendMailWithAttachment(final Email details);

}
