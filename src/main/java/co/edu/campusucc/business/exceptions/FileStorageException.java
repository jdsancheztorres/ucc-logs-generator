package co.edu.campusucc.business.exceptions;

public class FileStorageException extends Exception{

    /**
	 * 
	 */
	private static final long serialVersionUID = -2725482125539800060L;

	public FileStorageException(String s) {
        super(s);
    }

    public FileStorageException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
