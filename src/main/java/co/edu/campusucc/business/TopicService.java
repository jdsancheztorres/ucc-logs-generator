package co.edu.campusucc.business;

import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.campusucc.api.payload.TopicConverter;
import co.edu.campusucc.domain.Topic;
import co.edu.campusucc.infra.spring.data.TopicRepository;
/**
 * Service class used for handling operations associated to the topics
 * related to one log user on the UCC Logs Generator project.
 * 
 * @author jose.sanchezt@campusucc.edu.co
 *
 */
@Service
public class TopicService implements ITopicService{
	
	@Autowired
    private TopicRepository topicRepository;

    public Stream<Topic> findAll(){
		return StreamSupport.stream(topicRepository.findAll().spliterator(), false).map(TopicConverter::toTopic);
	}
}
