package co.edu.campusucc.business;

import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.campusucc.api.payload.PositionConverter;
import co.edu.campusucc.domain.Position;
import co.edu.campusucc.infra.spring.data.PositionRepository;
/**
 * Service class used for handling operations associated to the positions
 * related to one user on on the UCC Logs Generator project.
 * 
 * @author jose.sanchezt@campusucc.edu.co
 *
 */
@Service
public class PositionService implements IPositionService{
	
	@Autowired
    private PositionRepository positionRepository;

    public Stream<Position> findAllOrderByName(){
		return StreamSupport.stream(positionRepository.findAllOrderByName().spliterator(), false).map(PositionConverter::toPosition);
	}
}
