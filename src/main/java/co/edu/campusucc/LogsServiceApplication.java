package co.edu.campusucc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * 
 * Initial Class in the application called UCCLogsGenerator.
 * 
 * @author jose.sanchezt@campusucc.edu.co
 * 
 *
 */

@SpringBootApplication
@ComponentScan(basePackages = "co.edu.campusucc")
@EnableAsync
public class LogsServiceApplication {
	public static void main(String[] args) {
		SpringApplication.run(LogsServiceApplication.class, args);
	}
}
