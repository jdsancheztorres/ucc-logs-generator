package co.edu.campusucc.config;

import java.util.logging.Logger;

import org.springframework.stereotype.Component;

import co.edu.campusucc.api.payload.APIUserLogRequest;
import co.edu.campusucc.common.UCCUtil;

@Component
public class EducationConfiguration {

	final Logger LOGGER = Logger.getLogger(EducationConfiguration.class.getName());

	public void execute(final APIUserLogRequest apiUserRequest) {

		final String USER_INFO = UCCUtil.retrieveName(apiUserRequest.getName(), apiUserRequest.getLastName())
				+ " Windows -" + "v.11 ";
		LOGGER.info(USER_INFO + "co.edu.campusucc.logs.architecture.ucc.StudentController::findAll -- Start");
		String request = "";
		LOGGER.info(USER_INFO + "co.edu.campusucc.logs.architecture.ucc.StudentController::findAll -- request:" + request);
		LOGGER.info(USER_INFO + "co.edu.campusucc.logs.architecture.ucc.StudentService::findAll -- Start");
		LOGGER.info(USER_INFO + "co.edu.campusucc.logs.architecture.ucc.StudentService::findAll -- End");
	}
}