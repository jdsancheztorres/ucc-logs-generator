package co.edu.campusucc.config;

import java.util.logging.Logger;

import co.edu.campusucc.api.payload.APIUserLogRequest;
import co.edu.campusucc.common.UCCUtil;

public class TradeConfiguration {

	final Logger LOGGER = Logger.getLogger(TradeConfiguration.class.getName());

	public void execute(final APIUserLogRequest apiUserRequest) {

		final String USER_INFO = UCCUtil.retrieveName(apiUserRequest.getName(), apiUserRequest.getLastName())
				+ " Windows -" + "v.11 ";

		LOGGER.info(USER_INFO  + "co.edu.campusucc.logs.architecture.ucc.TradeController::findAll -- Start");
		String request = "";
		LOGGER.info(USER_INFO + "co.edu.campusucc.logs.architecture.ucc.TradeController::findAll -- request:" + request);
		LOGGER.info(USER_INFO + "co.edu.campusucc.logs.architecture.ucc.TradeService::findAll -- Start");
		LOGGER.info(USER_INFO + "co.edu.campusucc.logs.architecture.ucc.TradeService::findAll -- End");
		LOGGER.info(USER_INFO  +  "co.edu.campusucc.logs.architecture.ucc.TradeController::findAll -- End");
	}
}
