package co.edu.campusucc.config;

import java.text.SimpleDateFormat;
import java.util.logging.Logger;

import co.edu.campusucc.api.payload.APIUserLogRequest;
import co.edu.campusucc.common.UCCUtil;

public class EntertainmentConfiguration {

	final Logger LOGGER = Logger.getLogger(EntertainmentConfiguration.class.getName());

	public void execute(final APIUserLogRequest apiUserRequest) {
		
		final String USER_INFO = UCCUtil.retrieveName(apiUserRequest.getName(), apiUserRequest.getLastName())
				+ " Windows -" + "v.11 ";
		
		LOGGER.info(USER_INFO + "co.com.josedanilosanz.logs.architecture.ucc.EntertainmentController::findAll -- Start");
		String request = "";
		LOGGER.info(USER_INFO + "co.edu.campusucc.logs.architecture.ucc.EntertainmentController::findAll -- request:" + request);
		LOGGER.info(USER_INFO + "co.edu.campusucc.logs.architecture.ucc.EntertainmentService::findAll -- Start");
		LOGGER.info(USER_INFO + "co.edu.campusucc.logs.architecture.ucc.EntertainmentService::findAll -- End");
		LOGGER.info(USER_INFO + "co.edu.campusucc.logs.architecture.ucc.EntertainmentController::findAll -- End");
	}
}
