package co.edu.campusucc.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.Getter;

@Component
@Getter
public class LogsGenerator {
	
	@Autowired
	private BankConfiguration bankConfiguration;
	
	@Autowired
	private GovermentConfiguration govermentConfiguration;
	
	@Autowired
	private EducationConfiguration educationConfiguration;
	
	@Autowired
	private HealthConfiguration healthConfiguration;
	
	@Autowired
	private TradeConfiguration tradeConfiguration;
	
	@Autowired
	private EntertainmentConfiguration entertainmentConfiguration;
}
