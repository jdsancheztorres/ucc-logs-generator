package co.edu.campusucc.config;

import java.text.SimpleDateFormat;
import java.util.logging.Logger;

import org.springframework.stereotype.Component;

import co.edu.campusucc.api.payload.APIUserLogRequest;
import co.edu.campusucc.common.UCCUtil;

@Component
public class GovermentConfiguration {
	
	final Logger LOGGER = Logger.getLogger(GovermentConfiguration.class.getName());

	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");

	public void execute(final APIUserLogRequest apiUserRequest){
		
		final String USER_INFO = UCCUtil.retrieveName(apiUserRequest.getName(), apiUserRequest.getLastName())
				+ " Windows -" + "v.11 ";
		
		LOGGER.info(USER_INFO + "co.edu.campusucc.logs.architecture.ucc.GovermentController::findAll -- Start");
		String request = "";
		LOGGER.info(USER_INFO + "co.edu.campusucc.logs.architecture.ucc.GovermentController::findAll -- request:" + request);
		LOGGER.info(USER_INFO + "co.edu.campusucc.logs.architecture.ucc.GovermentService::findAll -- Start");
		LOGGER.info(USER_INFO + "co.edu.campusucc.logs.architecture.ucc.GovermentService::findAll -- End");
		LOGGER.info(USER_INFO + "co.edu.campusucc.logs.architecture.ucc.GovermentController::findAll -- End");
	}
}
