package co.edu.campusucc.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@Configuration
public class UCCLogsConfig {

	@Bean
	public BankConfiguration getBankSchedulerComponent() {
		return new BankConfiguration();
	}

	@Bean
	public GovermentConfiguration getGovermentSchedulerComponent() {
		return new GovermentConfiguration();
	}

	@Bean
	public EducationConfiguration getEducationSchedulerComponent() {
		return new EducationConfiguration();
	}

	@Bean
	public HealthConfiguration getHealthSchedulerComponent() {
		return new HealthConfiguration();
	}

	@Bean
	public TradeConfiguration getTradeSchedulerComponent() {
		return new TradeConfiguration();
	}

	@Bean
	public EntertainmentConfiguration getEntertainmentSchedulerComponent() {
		return new EntertainmentConfiguration();
	}
}
