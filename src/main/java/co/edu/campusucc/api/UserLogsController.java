package co.edu.campusucc.api;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.edu.campusucc.api.payload.APIUserLogRequest;
import co.edu.campusucc.business.IUserLogService;
import co.edu.campusucc.common.Api;
import co.edu.campusucc.common.UCCUtil;
import co.edu.campusucc.common.enums.TopicEnum;
import co.edu.campusucc.config.LogsGenerator;
import co.edu.campusucc.domain.UserLog;

/**
 * Controller class used for creating and sending logs to someone.
 * 
 * @author jose.sanchezt@campusucc.edu.co
 *
 */
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/userlog")
public class UserLogsController {

	@Autowired
	private IUserLogService userLogsService;
	
	@Autowired
	private LogsGenerator logsGenerator;

	/**
	 * Logger
	 */
	static final Logger LOGGER = Logger.getLogger(UserLogsController.class.getCanonicalName());

	@PostMapping(value = "/", produces = "application/json")
	@CrossOrigin(origins = "*")
	public ResponseEntity<?> create(@RequestBody final APIUserLogRequest apiUserRequest)
			throws IOException, InterruptedException, ExecutionException {
		final String USER_INFO = UCCUtil.retrieveName(apiUserRequest.getName(), apiUserRequest.getLastName())
				+ " Windows -" + "v.11 ";
		LOGGER.info(USER_INFO + "UserController::create: | Creating information about the log user: " + apiUserRequest.toString());
		writeLogs(apiUserRequest);
		final Optional<UserLog> response = userLogsService.save(apiUserRequest);
		LOGGER.info(USER_INFO + "UserController::create: | Creating information about the log user: END ");
		return Api.ok(response);
	}

	private void writeLogs(final APIUserLogRequest apiUserRequest) {

		List<String> topics = apiUserRequest.getTopics();
		if (topics.size() > 0) {
			for (String topic : topics) {

				final TopicEnum topicEnum = TopicEnum.fetchValue(topic);
				switch (topicEnum) {

				case EDUCATION:
					logsGenerator.getEducationConfiguration().execute(apiUserRequest);
					break;

				case HEALTH:
					logsGenerator.getHealthConfiguration().execute(apiUserRequest);
					break;

				case BANKS:
					logsGenerator.getBankConfiguration().execute(apiUserRequest);
					break;

				case TRADE:
					logsGenerator.getTradeConfiguration().execute(apiUserRequest);
					break;

				case ENTERTAINNMENT:
					logsGenerator.getEntertainmentConfiguration().execute(apiUserRequest);
					break;

				case GOVERMENT:
					logsGenerator.getGovermentConfiguration().execute(apiUserRequest);
					break;
				}
				
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
