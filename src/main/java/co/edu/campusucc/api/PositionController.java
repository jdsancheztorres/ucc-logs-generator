package co.edu.campusucc.api;

import java.util.logging.Logger;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.edu.campusucc.api.payload.APIPositionResponse;
import co.edu.campusucc.business.IPositionService;
import co.edu.campusucc.common.Api;
/**
 * Controller class used for handling operations associated to the positions
 * related to one user on the UCCLogsGenerator project.
 * 
 * @author jose.sanchezt@campusucc.edu.co
 *
 */

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/position")
public class PositionController {

	@Autowired
	private IPositionService positionService;
	
	/**
	 * Logger
	 */
	static final Logger LOGGER = Logger.getLogger(PositionController.class.getCanonicalName());
	
	@RequestMapping(value = { "/" }, method = RequestMethod.GET, produces = "application/json")
	@CrossOrigin(origins = "*")
	public ResponseEntity<Stream<APIPositionResponse>> findAll() {
		LOGGER.info("PositionController::findAllOrderByName");
		return Api.ok(positionService.findAllOrderByName().map(APIPositionResponse::from));
	}
}
