package co.edu.campusucc.api.payload;

import co.edu.campusucc.domain.Position;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class APIPositionResponse {
	
	private Long id;

    private String name;
    
    public static APIPositionResponse from(Position position) {
		
    	APIPositionResponse taskResponse = new APIPositionResponse();
		taskResponse.setId(position.getId());
		taskResponse.setName(position.getName());
	
        return taskResponse;
    }
}
