package co.edu.campusucc.api.payload;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.edu.campusucc.domain.Topic;
import co.edu.campusucc.infra.spring.data.TopicEntity;

/**
 * Converter class used for converting between Topic and TopicEntity
 * classes.
 * 
 * @author jose.sanchezt@campusucc.edu.co
 *
 */
public class TopicConverter {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TopicConverter.class);

    public static TopicEntity toTopicEntity(final Topic request) {
    	
    	final TopicEntity entity = new TopicEntity();
        entity.setId(request.getId());
        entity.setName(request.getName());
        entity.setDescription(request.getDescription());

        LOGGER.info("entity es " + entity.toString());
        return entity;
    }

    public static Topic toTopic(final TopicEntity request) {
    	
    	final Topic user = new Topic();
    	user.setId(request.getId());
        user.setName(request.getName());
        user.setDescription(request.getDescription());
       
        LOGGER.info("User is: " + request.toString());
        return user;
    }

    public static TopicEntity toTopicEntity(final APITopicRequest request) {
    	
    	final TopicEntity entity = new TopicEntity();
        entity.setId(request.getId());
        entity.setName(request.getName());
        entity.setDescription(request.getDescription());
        
		return entity;
    	
    }
}
