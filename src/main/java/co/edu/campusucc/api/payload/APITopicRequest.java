package co.edu.campusucc.api.payload;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class APITopicRequest {

	private Long id;

    private String name;
    
    private String description;
}
