
package co.edu.campusucc.api.payload;

import co.edu.campusucc.domain.UserLog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.edu.campusucc.infra.spring.data.UserLogEntity;

public class UserLogConverter {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserLogConverter.class);

    public static UserLogEntity toUserEntity(UserLog request) {
    	
        UserLogEntity entity = new UserLogEntity();
        entity.setEmail(request.getEmail());
        entity.setName(request.getName());
        entity.setLastName(request.getLastName());

        LOGGER.info("entity es " + entity.toString());
        return entity;
    }

    public static UserLog toUser(UserLogEntity request) {
    	
        UserLog user = new UserLog();
        user.setEmail(request.getEmail());
        user.setName(request.getName());
        user.setLastName(request.getLastName());
       
        LOGGER.info("User is: " + request.toString());
        return user;
    }

    public static UserLogEntity toUserEntity(APIUserLogRequest request) {
    	
    	UserLogEntity user = new UserLogEntity();
        user.setEmail(request.getEmail());
        user.setName(request.getName());
        user.setLastName(request.getLastName());	
        user.setCompany(request.getCompany());

		return user;
    	
    }

}
