package co.edu.campusucc.api.payload;

import co.edu.campusucc.domain.Topic;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class APITopicResponse {

	private Long id;

	private String name;

	private String description;

	public static APITopicResponse from(final Topic topic) {

		final APITopicResponse taskResponse = new APITopicResponse();
		taskResponse.setId(topic.getId());
		taskResponse.setName(topic.getName());
		taskResponse.setName(topic.getDescription());

		return taskResponse;
	}
}
