package co.edu.campusucc.api.payload;

import java.util.List;

import com.google.gson.GsonBuilder;

import co.edu.campusucc.domain.UserLog;
import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class APIUserLogRequest{

	private String name;

	private String lastName;

	private String email;
	
	private Long userPosition;
	
	private String company;
	
	private List<String> topics;
	
	
	public static APIUserLogRequest from(UserLog user) {
		APIUserLogRequest apiUserLogRequest = new APIUserLogRequest();
		apiUserLogRequest.setEmail(user.getEmail());
		apiUserLogRequest.setLastName(user.getLastName());
		apiUserLogRequest.setName(user.getName());
  
        return apiUserLogRequest;
    }
	
	public String toString() {
		return new GsonBuilder().setPrettyPrinting().create().toJson(this);
    }
}
