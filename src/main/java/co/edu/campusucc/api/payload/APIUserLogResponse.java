package co.edu.campusucc.api.payload;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class APIUserLogResponse {

	private Long id;

	private String userName;

	private String password;

	private String name;

	private String lastName;

	private String email;


}
