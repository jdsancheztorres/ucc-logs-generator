package co.edu.campusucc.api.payload;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.edu.campusucc.domain.Position;
import co.edu.campusucc.infra.spring.data.PositionEntity;


/**
 * Converter class used for converting between Position and PositionEntity
 * classes.
 * 
 * @author jose.sanchezt@campusucc.edu.co
 *
 */
public class PositionConverter {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PositionConverter.class);

    public static PositionEntity toPositionEntity(Position request) {
        final PositionEntity entity = new PositionEntity();
        entity.setId(request.getId());
        entity.setName(request.getName());
        LOGGER.info("entity es " + entity.toString());
        return entity;
    }

    public static Position toPosition(PositionEntity request) {
        final Position position = new Position();
        position.setId(request.getId());
        position.setName(request.getName());
        LOGGER.info("Position is " + request.toString());
        return position;
    }
}
