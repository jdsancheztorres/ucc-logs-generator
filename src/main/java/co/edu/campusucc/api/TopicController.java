package co.edu.campusucc.api;

import java.util.logging.Logger;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.edu.campusucc.api.payload.APITopicResponse;
import co.edu.campusucc.business.ITopicService;
import co.edu.campusucc.common.Api;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/topic")
public class TopicController {

	@Autowired
	private ITopicService topicService;

	/**
	 * Logger
	 */
	static final Logger LOGGER = Logger.getLogger(PositionController.class.getCanonicalName());

	@RequestMapping(value = { "/" }, method = RequestMethod.GET, produces = "application/json")
	@CrossOrigin(origins = "*")
	public ResponseEntity<Stream<APITopicResponse>> findAll() {
		LOGGER.info("PositionController::findAllOrderByName");
		return Api.ok(topicService.findAll().map(APITopicResponse::from));
	}

}
