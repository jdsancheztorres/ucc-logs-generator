package co.edu.campusucc.infra.spring.data;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
/**
 * Repository interface used for handling operations associated to the positions
 * related to one user on the UCCLogsGenerator project.
 * 
 * @author jose.sanchezt@campusucc.edu.co
 *
 */
public interface PositionRepository extends CrudRepository<PositionEntity, Long> {
	@Query(value="SELECT position FROM PositionEntity position order by position.name")
    public Iterable<PositionEntity> findAllOrderByName();
}
