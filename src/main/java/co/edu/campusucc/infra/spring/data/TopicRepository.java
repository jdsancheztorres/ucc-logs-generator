package co.edu.campusucc.infra.spring.data;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface TopicRepository extends CrudRepository<TopicEntity, Long>{
	
	@Query(value="SELECT topic FROM TopicEntity topic where topic.name = :topicName")
    public Optional<TopicEntity> findByName(@Param("topicName") String topicName);
}
