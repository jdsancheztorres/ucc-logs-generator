package co.edu.campusucc.infra.spring.data;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;

import java.io.Serializable;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "position")
public class PositionEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "id")
    @GeneratedValue
    private Long id;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "position")
    private Set<UserLogEntity> users;

    @ManyToOne
    @JoinColumn(name="position_type_id", nullable=false)
    private PositionTypeEntity positionType;
}
