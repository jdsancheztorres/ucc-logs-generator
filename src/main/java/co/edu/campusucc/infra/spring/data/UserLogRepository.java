package co.edu.campusucc.infra.spring.data;

import org.springframework.data.repository.CrudRepository;
public interface UserLogRepository extends CrudRepository<UserLogEntity, Long> {
}
