package co.edu.campusucc.common;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UCCUtil {

	public static String retrieveName(String name, String lastName) {

		String result = "";
		final String[] names = name.split("\\s+");
		final String[] lastNames = lastName.split("\\s+");
		final String defName = names.length > 0 ? "" + names[0].charAt(0) : name;
		final String defLastName = lastNames.length > 0 ? "" + lastNames[0] : lastName;

		result = normalizeString(defName + "" + defLastName);

		return result;
	}

	public static List<String> readFileByCriteria(String fileName, String stringToSearch) {
		
		List<String> listString = new ArrayList();
		try {
			listString = Files.lines(Paths.get(fileName))
									.filter(l -> l.contains(stringToSearch))
									.collect(Collectors.toList());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listString;
	}
	
	public static List<String> writeFile(String fileName, List<String> contentList) {
		
		List<String> listString = new ArrayList();
		try {
			Files.write(Paths.get(fileName), contentList,
					StandardCharsets.UTF_8,
			        StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listString;
	}
	
	public static String normalizeString(String str) {
		str = Normalizer.normalize(str, Normalizer.Form.NFKD);
		return str.replaceAll("[^a-z,^A-Z,^0-9]", "");
	}
	
	public static void main(String args[]) {
		List<String> archivo = UCCUtil.readFileByCriteria("/home/jdst/tesis/PruebaPostgresql/ucc-logs-generator/logs/revisionLogs.log", "JSanchez");
		UCCUtil.writeFile("/home/jdst/tesis/PruebaPostgresql/ucc-logs-generator/logs/DanilitoLIndo.log", archivo);
	}
}
