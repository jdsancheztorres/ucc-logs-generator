package co.edu.campusucc.common;

import java.util.function.Function;
import java.util.stream.Stream;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class Api {
	public static <T> ResponseEntity<T> ok(T resp) {
		return ResponseEntity.status(HttpStatus.OK).body(resp);
	}

	public static <T, U> ResponseEntity<Stream<U>> ok(Stream<T> resp, Function<T, U> f) {
		return ResponseEntity.status(HttpStatus.OK).body(resp.map(f));
	}
}
