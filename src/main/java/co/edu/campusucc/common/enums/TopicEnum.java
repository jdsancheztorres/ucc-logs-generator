package co.edu.campusucc.common.enums;

import lombok.Getter;

@Getter
public enum TopicEnum {
	
	EDUCATION("Educación"),
	HEALTH("Salud"),
	BANKS("Bancos"),
	TRADE("Comercio"),
	ENTERTAINNMENT("Entretenimiento"),
	GOVERMENT("Gobierno");
	
	private final String topic;

    private TopicEnum(final String topic) {
        this.topic = topic;
    }
    
    public static TopicEnum fetchValue(String constant) {
		for (TopicEnum topic : TopicEnum.values()) {
			if (topic.getTopic().equals(constant)) {
				return topic;
			}
		}
		return null;
	}
}
