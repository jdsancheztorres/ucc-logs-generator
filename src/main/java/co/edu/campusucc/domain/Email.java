package co.edu.campusucc.domain;

import java.util.List;

import com.google.gson.GsonBuilder;

import lombok.Data;

@Data
public class Email {
	
	private String recipient;
    private String msgBody;
    private String subject;
    private String attachment;
    private String name;
    private String lastName;
    private List<String> topics;
    
    public String toString() {
		return new GsonBuilder().setPrettyPrinting().create().toJson(this);
    }
}
