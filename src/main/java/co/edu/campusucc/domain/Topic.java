package co.edu.campusucc.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Topic {
	
	private Long id;

    private String name;
    
    private String description;
}
