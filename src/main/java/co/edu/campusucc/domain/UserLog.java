package co.edu.campusucc.domain;

import com.google.gson.GsonBuilder;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserLog {

    private String name;

    private String lastName;

    private String email;
    
    private Long position_id;
    
    private String company;
    
    public String toString() {
		return new GsonBuilder().setPrettyPrinting().create().toJson(this);
    }
}
