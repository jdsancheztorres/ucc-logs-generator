package co.edu.campusucc.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Position {
	
	private Long id;

    private String name;
}
