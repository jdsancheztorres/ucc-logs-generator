package co.edu.campusucc.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PositionType {

    private Long id;

    private String name;
}
